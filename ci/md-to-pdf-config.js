const { markedTwemoji } = require('marked-twemoji');

module.exports = {
  body_class: 'markdown-body',
  pdf_options: {
    preferCSSPageSize: true,
    margin: 0,
    printBackground: true,
  },
  marked_options: {
    gfm: true,
  },
  css: `
    @page {
      size: 8in 37in;
    }
    .page-break {
      page-break-after: always;
    }
    .markdown-body {
      font-size: 14px;
      padding: 16mm;
    }
    .markdown-body pre > code {
      white-space: pre-wrap;
    }
    .emoji {
      background: transparent;
      height: 1em;
      margin: 0 0.05em 0 0.1em !important;
      vertical-align: -0.1em;
      width: 1em;
    }`,
  marked_extensions: [markedTwemoji],
};
