# :sparkles: Senior DevOps Engineer

<!-- delete_before_rendering -->
## [:pencil: PDF version of resume available on Releases page](https://gitlab.com/dshatohin/resume/-/releases)

<img title="That's me" src="./assets/photo.jpg" height=200>
<img title="That's CKA badge" src="./assets/cka.png" height=130>
<img title="That's Cilium service mesh badge" src="./assets/cilium-service-mesh.png" height=130>
<img title="That's Cilium cluster mesh badge" src="./assets/cilium-cluster-mesh.png" height=130>

### Denis Shatokhin

:round_pushpin: **Helsinki, Finland**  
[:email: d_shatokhin@outlook.com](mailto:d_shatokhin@outlook.com)  
[:telephone_receiver: +358443004182](tel:+358443004182)  
[:paperclip: LinkedIn](https://www.linkedin.com/in/denis-shatokhin-63270285)

## :wave: About me

I'm a purposeful and enthusiastic engineer with specialization
in Kubernetes and cloud native applications.
But not afraid to get my hands dirty with bare metal, ansible and bash.

Always trying to reduce complexity of anything I build -
sometimes single VM is just better than cluster of orchestrated containers.

Strongly advocate for total **IaC** and automation of everything -
even the **PDF** version of this resume was build automatically via **CI**
from a markdown version stored in my gitlab repo
[dshatohin/resume](https://gitlab.com/dshatohin/resume)

Ready to share my experience and knowledge in any available form.

Speaking :gb: English and :ru: Russian languages.

[:trophy: Certified Kubernetes Administrator](https://www.credly.com/badges/dc833ff4-a6c2-46e9-a52d-7fd3ebd198c8)  
[:scroll: Cilium Service Mesh](https://www.credly.com/badges/4c174411-b54b-4bfc-b856-52f02583e3f4)  
[:scroll: Cilium Cluster Mesh](https://www.credly.com/badges/97ef7356-a817-4824-92de-b921bca64ad1)  
[:rocket: My Gitlab Project](https://gitlab.com/trac-app-k8s)

## :computer: My skills

There is a short list of my main skills:

- `Kubernetes`
- `Azure`
- `AWS`
- `Terraform`
- `Ansible`
- `Prometheus`
- `GitOps`
- `CI/CD`
- `Openstack`

## :clipboard: Employment history

### :office: DevOps Engineer, [Eficode](https://eficode.com/?hsLang=en)

#### February 2023 - present

- Managing resources in AWS and Azure clouds
- Preparing for the Azure 104 certification exam
- Learning about Cilium and deploying kube-proxy'less Kubernetes clusters
- Doing every Kubernetes related tasks in my team
- Writing shared library for Jenkins to reduce amount of Groovy copy-paste
- Implementing IaC for Jenkins pipelines via Jenkins Job Builder
- Keeping Terraform repo DRY with Terragrunt
- Finding weak spots in infrastructure code and implementing solutions
- Working for different clients with a wide variety of tools
- Trying to use Zabbix and not complaining too much about it

> Working in a consultancy is an opportunity to learn more about
> diverse range of tools and approaches used by many companies.
>
> Here I left the comfort zone and faced few challenges -
> monitoring Kubernetes with Zabbix, deploying helm-charts via Ansible
> and using SCM with a separate build system.

---

### :office: Senior DevOps Engineer, [Selectel](https://selectel.ru/en)

#### August 2021 - February 2023

- Planning and building infrastructure from scratch for our new PaaS product - Container Registry
- Running almost everything in Kubernetes
- Working together with Golang developers to achieve a tight fit between infrastructure and code
- Developing Helm Charts, Terraform modules, Ansible roles
- Building a custom logging solution based on Fluent-bit and Clickhouse to calculate client's traffic based on Envoy access logs
- Setting up separate Envoy dataplane layer for Contour ingress-controller controlplane with BGP connection to border routers
- Monitoring everything via VictoriaMetrics and visualizing in Grafana with custom dashboards
- Working with NATS messaging system
- Writing custom parser in Javascript for Alertmanager messages
- Pushing ArgoCD to it's limits with my own plugins
- Contributing to Bitnami helm-charts and Openstack Cloud Controller Manager
- Going from Beta to General Availability for our external clients
- Supporting and taking part in developing our main product - Managed Kubernetes Service
- Mastering my skills in everything I've learnt so far
- Falling in love with Kubernetes and acquiring CKA certificate as a result

> Because we were working with Openstack cloud and developed by us services -
> every terraform module should be written from scratch.
> Reading Golang code became main source for insight when debugging errors.
>
> Here I met my next 3 favorite technologies Envoy, Clickhouse and VictoriaMetrics.

---

### :office: System Administrator / DevOps Engineer, [Selectel](https://selectel.ru/en)

#### March 2019 - August 2021

- Getting exposed to Ansible, Terraform and Kubernetes
- Following strict IaC for everything (even legacy Windows PCs)
- Proposing and enabling dynamic inventory for Ansible repo (getting inventory from Netbox)
- Starting migration from Zabbix to Prometheus with self written service discovery (first Golang project)
- Constructing Grafana dashboards with all sorts of panels and visualizations
- Contributing to `modbus_exporter` for Prometheus
- Moving all workload to Kubernetes
- Utilizing gitops approach for deploy
- Sharing knowledge among other sysadmins in my team by doing webinars
- Containerizing Python codebase with multistage builds
- Building the first pipeline for python developers (no more manual FTP upload)
- Implementing dynamic environments for every merge request

> Our team was on the edge between IT and OT (operational technology) -
> on the one hand we had servers, virtual machines, databases and
> on the other - industrial equipment like diesel generators or UPS cluster for the whole server room.
> And we have to provide robust monitoring solution regardless of the differences in protocols.
>
> Back then I realized that old way of supporting infrastructure not working anymore.
> The phrase "treat infrastructure like cattle instead of pets"
> have became my motto till this day.

---

### :office: System Engineer, [Selectel](https://selectel.ru/en)

#### July 2014 - March 2019

- Working as a remote hands for our internal and external clients in our data centers
- Installing servers in server racks
- Organizing optical and cooper cross connections between equipment
- Basic configuration of Linux and Windows server, network routers, switches and firewalls
- Experience with a wide variety of technologies
- Insight into physical layers of Openstack and VMware clouds installations

> Listening never ending white noise of server rooms I truly understood the
> meaning of the phrase "the cloud is just someone else's computer".
